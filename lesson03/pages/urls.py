from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('ab/', views.AboutPageView.as_view(), name='about'),
]
